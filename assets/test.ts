// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
// import { Video } from "./video";
const {ccclass, property} = cc._decorator;
import WebvideoPlayer from "./webvideoPlayer";

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.String) url = '';
    webvideoPlayer:WebvideoPlayer = new WebvideoPlayer()
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        

        cc.resources.load(this.url, cc.Asset, (err, asset: any) => {
            
            this.webvideoPlayer.open(asset.nativeUrl,()=>{
                // this.node.getComponent(cc.Sprite).spriteFrame.setTexture());
                        this.setTexture()
            })
        })

        // setTimeout(() => {
        //     this.webvideoPlayer.open("data:video/mp4;base64,"+Video.video1,()=>{
        //         // this.node.getComponent(cc.Sprite).spriteFrame.setTexture());
        //                 this.setTexture()
        //     })
        // }, 3000);
    }

    setTexture(){
        let texture = this.webvideoPlayer.getTexture()
        let spriteFrame = new cc.SpriteFrame();
        // let image = new Image();
        // image.src = dataUrl;
        // texture.initWithElement(image);
        
        spriteFrame.setTexture(texture);
        this.node.getComponent(cc.Sprite).spriteFrame = spriteFrame
    }

    update (dt) {
        this.webvideoPlayer.update(dt)
        this.setTexture()
    }

    // ========= Methods =============



    
    // ========= Methods =============
}
